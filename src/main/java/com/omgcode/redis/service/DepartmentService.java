package com.omgcode.redis.service;

import com.omgcode.redis.model.Department;
import com.omgcode.redis.repository.redis.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    public void saveDepartment(Department department) {
        this.departmentRepository.save(department);
    }

    public void deleteDepartmentById(Integer departmentId) {
        this.departmentRepository.deleteById(departmentId);
    }
}
