package com.omgcode.redis.service;

import com.omgcode.redis.model.Employee;
import com.omgcode.redis.repository.redis.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public void persistEmployee(Employee employee) {
        employeeRepository.saveEmployee(employee);
    }
}
