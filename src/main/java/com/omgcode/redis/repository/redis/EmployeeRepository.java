package com.omgcode.redis.repository.redis;

import com.omgcode.redis.model.Employee;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Map;

@Repository
public class EmployeeRepository {
    private final String hashReference= "Employee";

    @Resource(name="redisTemplate")
    private HashOperations<String, Integer, Employee> hashOperations;

    public void saveEmployee(Employee employee) {
        hashOperations.put(hashReference, employee.getId(), employee);
    }

    public void saveAllEmployees(Map<Integer, Employee> map) {
        hashOperations.putAll(hashReference, map);
    }

    public Employee getOneEmployee(Integer id) {
        return hashOperations.get(hashReference, id);
    }

    public void updateEmployee(Employee emp) {
        hashOperations.put(hashReference, emp.getId(), emp);
    }

    public Map<Integer, Employee> getAllEmployees() {
        return hashOperations.entries(hashReference);
    }

    public void deleteEmployee(Integer id) {
        hashOperations.delete(hashReference, id);
    }

}
