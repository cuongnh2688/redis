package com.omgcode.redis.controller;


import com.omgcode.redis.model.Employee;
import com.omgcode.redis.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping("/save-employee")
    public ResponseEntity<String> saveEmployee(@RequestBody Employee employee) {
        this.employeeService.persistEmployee(employee);
        return ResponseEntity.ok("Create employee successfully!");
    }
}
