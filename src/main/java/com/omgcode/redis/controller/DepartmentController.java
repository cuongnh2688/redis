package com.omgcode.redis.controller;

import com.omgcode.redis.model.Department;
import com.omgcode.redis.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/department")
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @PostMapping("/save-Department")
    public ResponseEntity<String> saveDepartment(@RequestBody Department department) {
        this.departmentService.saveDepartment(department);
        return ResponseEntity.ok("Create employee successfully!");
    }

    @DeleteMapping("/delete-Department/{departmentId}")
    public ResponseEntity<String> deleteDepartment(@PathVariable Integer departmentId) {
        this.departmentService.deleteDepartmentById(departmentId);
        return ResponseEntity.ok("Delete employee successfully!");
    }
}
