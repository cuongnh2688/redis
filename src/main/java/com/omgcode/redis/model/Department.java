package com.omgcode.redis.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@RedisHash(value = "department", timeToLive = 3600) // 3600 seconds
public class Department {
    private Integer id;
    private String departmentName;
    private String description;
}
